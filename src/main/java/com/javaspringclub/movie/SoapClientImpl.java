package com.javaspringclub.movie;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;


import movie.wsdl.GetAllMoviesResponse;
import movie.wsdl.AddMovieRequest;
import movie.wsdl.AddMovieResponse;
import movie.wsdl.DeleteMovieRequest;
import movie.wsdl.DeleteMovieResponse;
import movie.wsdl.GetAllMoviesRequest;
import movie.wsdl.GetMovieByIdRequest;
import movie.wsdl.GetMovieByIdResponse;
import movie.wsdl.UpdateMovieRequest;
import movie.wsdl.UpdateMovieResponse;

public class SoapClientImpl extends WebServiceGatewaySupport implements SoapClient  {

	@Override
	public GetMovieByIdResponse getMovieById(String movieid) {
		GetMovieByIdRequest  request = new GetMovieByIdRequest();
		request.setMovieId(Long.valueOf(movieid, 10));
		GetMovieByIdResponse response = (GetMovieByIdResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws", request);
		return response ;
	}

	@Override
	public GetAllMoviesResponse getAllEntities() {
		GetAllMoviesRequest request = new GetAllMoviesRequest();
	 return (GetAllMoviesResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws", request);
		
	  
	   
	}

	@Override
	public DeleteMovieResponse deleteEntityById(String movieId) {
		DeleteMovieRequest request = new DeleteMovieRequest();
		request.setMovieId(Long.valueOf(movieId, 10));
		DeleteMovieResponse response =  (DeleteMovieResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws", request);
		return response;
	}

	@Override
	public AddMovieResponse addEntity(MovieEntity entity) {
		AddMovieRequest request = new AddMovieRequest();
		request.setCategory(entity.getCategory());
		request.setTitle(entity.getTitle());
		AddMovieResponse response = (AddMovieResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws", request);
		return response;
	}

	@Override
	public UpdateMovieResponse updateEntity(MovieEntity entity) {
		
		UpdateMovieRequest request = new UpdateMovieRequest();
		
		request.setCategory(entity.getCategory());
		request.setTitle(entity.getTitle());
		UpdateMovieResponse response =  (UpdateMovieResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws", request);
		return response;
	}

	
      
	
	
}
