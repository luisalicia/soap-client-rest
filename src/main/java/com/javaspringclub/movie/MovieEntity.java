package com.javaspringclub.movie;

import java.io.Serializable;


public class MovieEntity implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long movieId;
	private String title;
	private String category;
	
	public MovieEntity() {
			
		}
	
	public MovieEntity(String title, String category) {
		this.title= title;
		this.category = category;
	}
	
	
	public long getMovieId() {
		return movieId;
	}
	
	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	

}
