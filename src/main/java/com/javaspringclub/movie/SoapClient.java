package com.javaspringclub.movie;
import movie.wsdl.GetMovieByIdResponse;
import movie.wsdl.UpdateMovieResponse;
import movie.wsdl.AddMovieResponse;
import movie.wsdl.DeleteMovieResponse;
import movie.wsdl.GetAllMoviesResponse;



public interface SoapClient {
      
	public GetMovieByIdResponse getMovieById(String movieId); 
	public GetAllMoviesResponse getAllEntities();
	public DeleteMovieResponse deleteEntityById(String movieId);
	public AddMovieResponse addEntity(MovieEntity entity);
	public UpdateMovieResponse updateEntity(MovieEntity entity);
	
	
	 
}
