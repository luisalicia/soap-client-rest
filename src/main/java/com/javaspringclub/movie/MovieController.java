package com.javaspringclub.movie;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.javaspringclub.movie.MovieEntity;
import movie.wsdl.UpdateMovieResponse;
import movie.wsdl.GetMovieByIdResponse;
import movie.wsdl.AddMovieResponse;
import movie.wsdl.DeleteMovieResponse;
import movie.wsdl.GetAllMoviesResponse;


@RestController
public class MovieController {
	
	@Autowired
	SoapClient soapclient;
	
	@GetMapping("/getMoviebyid/{movieid}")
	public GetMovieByIdResponse getMovieById(@PathVariable("movieid") String movieid) {
		
		return this.soapclient.getMovieById(movieid);
		
	}
    
	@GetMapping("/getMovieAll")
    public GetAllMoviesResponse getMovieAll() {
		
		return soapclient.getAllEntities();
		
	}
	
	@DeleteMapping("/deletemovie/{movieid}")
	public DeleteMovieResponse deleteEntityById(@PathVariable("movieid") String movieid) {
		return this.soapclient.deleteEntityById(movieid);
	
	}
	
	@PostMapping("/addMovie")
	public AddMovieResponse addEntity(@RequestBody MovieEntity movie) {
		MovieEntity newMovieEntity = new MovieEntity(movie.getTitle(), movie.getCategory());
		return soapclient.addEntity(newMovieEntity);
	}
	
	@PutMapping("/update")
	public UpdateMovieResponse updateEntity(@RequestBody MovieEntity movie ) {
		return soapclient.updateEntity(movie);
	}
	
}
