package com.javaspringclub.movie;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class ConfigurationMovies {
	
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in
		// pom.xml
		marshaller.setContextPath("movie.wsdl");
		return marshaller;
	}

	@Bean
	public SoapClient  MovieClient(Jaxb2Marshaller marshaller) {
		SoapClientImpl Movie = new SoapClientImpl();
		Movie.setDefaultUri("http://localhost:8080/ws");
		Movie.setMarshaller(marshaller);
		Movie.setUnmarshaller(marshaller);
		return Movie;
	}

}
